#!/bin/bash
# coding: utf-8

# Este script es para que lo use el administrador
# Para crear las copias de seguridad de las particiones

# Requiere partclone instalado
# apt-get install partclone

# Requiere que el usuario "user" pueda ejecutar partclone como root sin password
# apt-get install sudo
# visudo
#     user    ALL=NOPASSWD: ALL

# Suponemos que el disco esta así:
#  /dev/sda1 Partición SO Windows
#  /dev/sda2 Partición DATOS Windows
#  /dev/sda3 Partición SO Linux
#  /dev/sda4 Partición DATOS Linux (/home)

# Los backups se crearán en:
#     /home/user/backup/windows
#        sda1.win.so.ntfs.partclone.gz
#        sda2.win.datos.ntfs.partclone.gz
#    /home/user/backup/linux
#        sda3.linux.so.ext4.partclone.gz
#        sda4.linux.datos.ext4.partclone.gz


# Borramos la pantalla
clear

# Menu principal
# Nota muy importante!!!!!:
#   Si quieres quitar alguna opción borra la línea
#   Si solo la comentas da error
whiptail --title "Escoja una opción" \
         --menu "" \
         12 60 6 \
         --nocancel \
         --clear \
         0 "Salir" \
         1 "Crear backup del sistema operativo Windows"  \
         2 "Crear backup de la partición de DATOS de Windows"  \
         3 "Crear backup del sistema operativo Linux"  \
         4 "Crear backup de la partición /home (DATOS) de Linux"  \
         5 "Crear carpetas para backup"  \
       2>temp


# Han pulsado la tecla OK
if [ "$?" = "0" ];then
   # Miramos que opción han elegido
   _return=$(cat temp)

   # 0 "Salir"
   if [ "$_return" = "0" ];then
      echo ""
   fi

   # 1 "Crear backup del sistema operativo Windows"
   if [ "$_return" = "1" ];then
        if (whiptail --title "Crear backup del sistema operativo Windows" \
                     --yesno "                   ¿Está seguro? \n                Tardará unos minutos." \
                     --yes-button "Si" \
                     --no-button "No" \
                     12 60);then
           echo ""
           sudo partclone.ntfs -c -s /dev/sda2 -N | gzip -c > backup/windows/sda2.win.so.ntfs.partclone.gz
        fi

        # Volvemos al menu principal
        bash asr-admin.bash
   fi

   # 2 "Crear backup de la partición de DATOS de Windows"
   if [ "$_return" = "2" ];then
        if (whiptail --title "Crear backup de la partición de DATOS de Windows" \
                     --yesno "                   ¿Está seguro? \n                Tardará unos minutos." \
                     --yes-button "Si" \
                     --no-button "No" \
                     12 60);then
           echo ""
           sudo partclone.ntfs -c -s /dev/sda3 -N | gzip -c > backup/windows/sda3.win.datos.ntfs.partclone.gz
         fi

         # Volvemos al menu principal
         bash asr-admin.bash
    fi

   # 3 "Crear backup del sistema operativo Linux"
   if [ "$_return" = "3" ];then
        if (whiptail --title "Crear backup del sistema operativo Linux" \
                     --yesno "                   ¿Está seguro? \n                Tardará unos minutos." \
                     --yes-button "Si" \
                     --no-button "No" \
                     12 60);then
           echo ""
           sudo partclone.ext4 -c -s /dev/sda5 -N | gzip -c > backup/linux/sda5.linux.so.ext4.partclone.gz
         fi

         # Volvemos al menu principal
         bash asr-admin.bash
    fi

   # 4 "Crear backup de la partición /home (DATOS) de Linux"
   if [ "$_return" = "4" ];then
        if (whiptail --title "Crear backup de la partición /home (DATOS) de Linux" \
                     --yesno "                   ¿Está seguro? \n                Tardará unos minutos." \
                     --yes-button "Si" \
                     --no-button "No" \
                     12 60);then
           echo ""
           sudo partclone.ext4 -c -s /dev/sda6 -N | gzip -c > backup/linux/sda6.linux.datos.ext4.partclone.gz
         fi

         # Volvemos al menu principal
         bash asr-admin.bash
    fi

   # 5 "Crear carpetas para backup"
   if [ "$_return" = "5" ];then
      mkdir backup backup/windows backup/linux

      whiptail --title "Carpetas creadas" \
               --clear \
               --msgbox "$(echo '             Pulse <Aceptar> para continuar')" \
               12 60

     # Volvemos al menu principal
     bash asr-admin.bash
   fi

# Cancel or ESC
# Han pulsado la tecla CANCEL o ESC
else
   echo ""
   #init 6
fi


# Borramos la pantalla
clear

# Borramos el archivo temporal
rm -f temp
